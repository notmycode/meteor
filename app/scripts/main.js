jQuery(document).ready(function() {
  // var's
  var homeSlider = $('.home-sl'),
    width = $(window).width(),
    height = $(window).height();

  //home sl init
  if (homeSlider.length > 0) {
    homeSlider.slick({
      dots: true,
      infinite: false,
      speed: 500,
      fade: true,
      cssEase: 'linear'
    });



    // align content vertical center
    var allSlidsContent = $('.home-sl__single > .main');
    var sliderHeight = $('.home-sl').height();
    if (allSlidsContent.length > 0) {
      for (var i = 0; i < allSlidsContent.length; i++) {
        var tempContent = $(allSlidsContent[i]);
        var topOffset = (sliderHeight - tempContent.height()) / 2;
        tempContent.css('padding-top', topOffset - 82);
      }
    }
    var patnersSlider = $('#parthners-slider');
    patnersSlider.slick({
      dots: false,
      infinite: true,
      variableWidth: true, 
      slidesToShow: 3,
      slidesToScroll: 3
    });
  }
});
